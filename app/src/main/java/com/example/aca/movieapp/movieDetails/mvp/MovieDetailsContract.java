package com.example.aca.movieapp.movieDetails.mvp;

import com.example.aca.movieapp.baseMVP.BasePresenter;
import com.example.aca.movieapp.baseMVP.BaseView;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.MovieDetails;

import java.util.List;

public interface MovieDetailsContract {

    interface View extends BaseView{
        void onDetailsLoaded(MovieDetails movieDetails);
    }

    interface Presenter extends BasePresenter{
        void getMovieDetails();
    }

    interface Interactor{

        interface OnMovieDetailsResponseListener {
            void onSuccessLoadedMovieDetails(MovieDetails movieDetails);
            void onUnSuccessLoadedMovieDetails(String errorMessage);
        }

        void getMovieDetails(OnMovieDetailsResponseListener onMovieResponseListener);
    }
}
