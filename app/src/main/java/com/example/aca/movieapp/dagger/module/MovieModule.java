package com.example.aca.movieapp.dagger.module;

import com.example.aca.movieapp.dagger.qualifier.ActivityScope;
import com.example.aca.movieapp.dagger.qualifier.ApplicationScope;
import com.example.aca.movieapp.movieMVP.MovieContract;

import dagger.Module;
import dagger.Provides;

@Module
public class MovieModule {

    private MovieContract.View view;
    private MovieContract.Interactor interactor;

    public MovieModule(MovieContract.View view, MovieContract.Interactor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @ActivityScope
    @Provides
    MovieContract.View provideView(){
        return this.view;
    }

    @ActivityScope
    @Provides
    MovieContract.Interactor provideInteractor(){
        return this.interactor;
    }
}
