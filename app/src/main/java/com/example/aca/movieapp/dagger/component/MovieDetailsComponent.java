package com.example.aca.movieapp.dagger.component;

import com.example.aca.movieapp.dagger.module.MovieDetailsModule;
import com.example.aca.movieapp.dagger.qualifier.ActivityScope;
import com.example.aca.movieapp.model.MovieDetails;
import com.example.aca.movieapp.movieDetails.MovieDetailsActivity;
import com.example.aca.movieapp.movieDetails.mvp.MovieDetailsPresenter;
import com.example.aca.movieapp.movieMVP.MoviePresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = NetComponent.class, modules = {MovieDetailsModule.class})
public interface MovieDetailsComponent {
    MovieDetailsPresenter expose();
    void inject(MovieDetailsActivity mainActivity);
}
