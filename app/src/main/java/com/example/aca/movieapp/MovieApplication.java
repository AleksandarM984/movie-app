package com.example.aca.movieapp;

import android.app.Application;

import com.example.aca.movieapp.dagger.component.DaggerNetComponent;
import com.example.aca.movieapp.dagger.component.NetComponent;
import com.example.aca.movieapp.dagger.module.AppModule;
import com.example.aca.movieapp.dagger.module.NetModule;

public class MovieApplication extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BuildConfig.BASE_API_URL))
                .build();
    }

    public NetComponent getNetComponent() {
        return netComponent;
    }
}
