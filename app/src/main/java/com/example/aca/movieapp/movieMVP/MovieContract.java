package com.example.aca.movieapp.movieMVP;

import com.example.aca.movieapp.baseMVP.BaseView;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.SearchMovie;

import java.util.List;

public interface MovieContract {

    interface View extends BaseView{
        void onTopRatedMovieLoaded(List<Movie> topMovies);
        void onSearchMovieLoaded(List<SearchMovie> movies);
    }

    interface Presenter{
        void getTopRatedMovie();
    }

    interface Interactor{

        interface OnMovieResponseListener{
            void onSuccessLoadedMovie(List<Movie> movies);
            void onSuccessSearchMovie(List<SearchMovie> movies);
            void onUnSuccessLoadedMovie(String errorMessage);
        }

        void getTopRatedMovie(OnMovieResponseListener onMovieResponseListener);
    }
}
