package com.example.aca.movieapp.apiService;

import com.example.aca.movieapp.model.FavoriteMovie;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.MovieDetails;
import com.example.aca.movieapp.model.Payload;
import com.example.aca.movieapp.model.SearchMovie;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieService {

    @GET("movie/top_rated")
    Observable<Payload<Movie>> getTopRatedMovies(@Query("api_key") String apiKey, @Query("page") int page);

    @GET("search/company")
    Observable<Payload<SearchMovie>> searchMovies(@Query("api_key") String apiKey, @Query("query") String queryValue, @Query("page") int page);

    @GET("movie/{movieId}/credits")
    Observable<MovieDetails> getMoviesDetails(@Path("movieId") int movieId, @Query("api_key") String apiKey);

    @FormUrlEncoded
    @POST("account/{account_id}/favorite")
    Call<Object> markLikeFavorite(@Path("account_id") int accountId, @Body FavoriteMovie favoriteMovie);

    @GET("/account/{account_id}/favorite/movies")
    Observable<Movie> getFavoriteMovies(@Path("account_id") int accountId);
}
