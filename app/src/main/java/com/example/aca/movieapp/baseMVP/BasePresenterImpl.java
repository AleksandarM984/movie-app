package com.example.aca.movieapp.baseMVP;

public class BasePresenterImpl implements BasePresenter {

    private BaseView baseView;

    public BasePresenterImpl(BaseView baseView) {
        this.baseView = baseView;
    }

    @Override
    public void errorMsg(String errorMsg) {
        baseView.showErrorLoadingDataMessage(errorMsg);
    }
}
