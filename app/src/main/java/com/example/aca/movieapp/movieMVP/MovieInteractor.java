package com.example.aca.movieapp.movieMVP;

import com.example.aca.movieapp.BuildConfig;
import com.example.aca.movieapp.apiService.MovieService;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.Payload;
import com.example.aca.movieapp.model.SearchMovie;
import com.example.aca.movieapp.utility.Util;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MovieInteractor implements MovieContract.Interactor {

    private int page = 1;
    @Inject
    Retrofit retrofit;


    @Inject
    public MovieInteractor() {
    }

    @Override
    public void getTopRatedMovie(final OnMovieResponseListener onMovieResponseListener) {
        if (Util.isOnline()) {
            getDataFromServer(onMovieResponseListener);
        }
        else
            getDataFromLocalDB();
    }

    //ToDo use Room lib for store data on device
    private void getDataFromLocalDB() {

    }

    private void getDataFromServer(final OnMovieResponseListener onMovieResponseListener) {
        retrofit.create(MovieService.class).getTopRatedMovies(BuildConfig.DEFAULT_API_KEY, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Payload<Movie>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Payload<Movie> movieResponse) {
                        page++;
                        onMovieResponseListener.onSuccessLoadedMovie(movieResponse.getResults());
                    }

                    @Override
                    public void onError(Throwable e) {
                        onMovieResponseListener.onUnSuccessLoadedMovie("error");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void searchMovie(String queryValue, final OnMovieResponseListener onMovieResponseListener) {
        retrofit.create(MovieService.class).searchMovies(BuildConfig.DEFAULT_API_KEY, queryValue, 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Payload<SearchMovie>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Payload<SearchMovie> movieResponse) {
                        onMovieResponseListener.onSuccessSearchMovie(movieResponse.getResults());
                    }

                    @Override
                    public void onError(Throwable e) {
                        onMovieResponseListener.onUnSuccessLoadedMovie(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
