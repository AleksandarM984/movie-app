package com.example.aca.movieapp.movieDetails.mvp;

import com.example.aca.movieapp.BuildConfig;
import com.example.aca.movieapp.apiService.MovieService;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.MovieDetails;
import com.example.aca.movieapp.model.Payload;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MovieDetailsInteractor implements MovieDetailsContract.Interactor {

    @Inject
    Retrofit retrofit;


    @Inject
    public MovieDetailsInteractor() {
    }

    @Override
    public void getMovieDetails(final OnMovieDetailsResponseListener onMovieDetailsResponseListener) {
        retrofit.create(MovieService.class).getMoviesDetails(19404, BuildConfig.DEFAULT_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MovieDetails>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(MovieDetails movieDetails) {
                            onMovieDetailsResponseListener.onSuccessLoadedMovieDetails(movieDetails);
                    }

                    @Override
                    public void onError(Throwable e) {
                            onMovieDetailsResponseListener.onUnSuccessLoadedMovieDetails(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
