package com.example.aca.movieapp.dagger.module;

import android.app.Application;

import com.example.aca.movieapp.dagger.qualifier.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @ApplicationScope
    @Provides
    Application provideApplication() {
        return application;
    }
}
