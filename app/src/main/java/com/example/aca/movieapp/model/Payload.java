package com.example.aca.movieapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Payload<T> {

    private int page;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;

    private List<T> results;

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<T> getResults() {
        return results;
    }
}
