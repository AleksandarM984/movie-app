package com.example.aca.movieapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.aca.movieapp.callback.OnLoadMoreListener;
import com.example.aca.movieapp.callback.OnMovieItemClickListener;
import com.example.aca.movieapp.dagger.component.DaggerMovieComponent;
import com.example.aca.movieapp.dagger.component.NetComponent;
import com.example.aca.movieapp.dagger.module.MovieModule;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.SearchMovie;
import com.example.aca.movieapp.movieDetails.MovieDetailsActivity;
import com.example.aca.movieapp.movieMVP.MovieContract;
import com.example.aca.movieapp.movieMVP.MovieInteractor;
import com.example.aca.movieapp.movieMVP.MoviePresenter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MovieContract.View, OnLoadMoreListener, OnMovieItemClickListener {


    public static final String MOVIE = "MOVIE";
    private List<Movie> movies = new ArrayList<>();
    private MovieAdapter movieAdapter;

    @BindView(R.id.moviesRV)
    RecyclerView moviesRV;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    Retrofit retrofit;
    @Inject
    MoviePresenter moviePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        resolveDaggerInjection();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        moviesRV.setHasFixedSize(true);
        moviesRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        movieAdapter = new MovieAdapter(movies, moviesRV, this, this);
        moviesRV.setAdapter(movieAdapter);
        moviePresenter.getTopRatedMovie();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.allMovie) {
            // Handle the camera action
        } else if (id == R.id.favoriteMovie) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void showLoadingProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorLoadingDataMessage(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTopRatedMovieLoaded(List<Movie> topMovies) {
        movieAdapter.setLoaded();
        movies.addAll(topMovies);
        movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSearchMovieLoaded(List<SearchMovie> movies) {
        StringBuilder result = new StringBuilder("");
        for (SearchMovie searchMovie : movies) {
            String lineContent = searchMovie.getName().concat("\n");
            result.append(lineContent);
        }
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        Toast.makeText(this, "Mislio sam da osvezim postojecu listu ali sam kasno shvatio da nije isti response " +
                "kada se radi get top reated moview i kada se radi search movie", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadMore() {
        showLoadingProgress();
        moviePresenter.getTopRatedMovie();
    }

    @Override
    public void onClickMovie(Movie movie) {
        Intent intent = new Intent(this, MovieDetailsActivity.class);
        intent.putExtra(MOVIE, movie);
        startActivity(intent);
    }

    @OnTextChanged(R.id.searchMovieET)
    public void onSearchInputChange(Editable editable) {
        progressBar.setVisibility(View.VISIBLE);
        String queryValue = editable.toString();
        moviePresenter.searchMovie(queryValue);
    }

    private void resolveDaggerInjection() {
        NetComponent netComponent = ((MovieApplication) getApplication()).getNetComponent();
        DaggerMovieComponent.builder()
                .netComponent(netComponent)
                .movieModule(new MovieModule(this, new MovieInteractor()))
                .build()
                .inject(this);
    }
}
