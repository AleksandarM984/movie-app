package com.example.aca.movieapp.model;

import com.google.gson.annotations.SerializedName;

public class FavoriteMovie {

    @SerializedName("media_type")
    private String mediaType;
    @SerializedName("media_id")
    private String mediaId;
    private Boolean favorite;

    public FavoriteMovie(String mediaType, String mediaId, Boolean favorite) {
        this.mediaType = mediaType;
        this.mediaId = mediaId;
        this.favorite = favorite;
    }
}
