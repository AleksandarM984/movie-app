package com.example.aca.movieapp.movieDetails.mvp;

import com.example.aca.movieapp.baseMVP.BasePresenterImpl;
import com.example.aca.movieapp.model.MovieDetails;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class MovieDetailsPresenter extends BasePresenterImpl implements MovieDetailsContract.Presenter, MovieDetailsContract.Interactor.OnMovieDetailsResponseListener {

    private MovieDetailsContract.View movieDetailsView;

    @Inject
    Retrofit retrofit;
    @Inject
    MovieDetailsInteractor movieDetailsInteractor;

    @Inject
    public MovieDetailsPresenter(MovieDetailsContract.View movieDetailsView) {
        super(movieDetailsView);
        this.movieDetailsView = movieDetailsView;
    }

    @Override
    public void getMovieDetails() {
        movieDetailsView.showLoadingProgress();
        movieDetailsInteractor.getMovieDetails(this);
    }

    @Override
    public void onSuccessLoadedMovieDetails(MovieDetails movieDetails) {
        movieDetailsView.hideLoadingProgress();
        movieDetailsView.onDetailsLoaded(movieDetails);

    }

    @Override
    public void onUnSuccessLoadedMovieDetails(String errorMessage) {

    }
}
