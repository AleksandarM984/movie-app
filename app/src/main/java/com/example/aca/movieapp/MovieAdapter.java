package com.example.aca.movieapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aca.movieapp.callback.OnLoadMoreListener;
import com.example.aca.movieapp.callback.OnMovieItemClickListener;
import com.example.aca.movieapp.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private List<Movie> movies;

    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnMovieItemClickListener onMovieItemClickListener;

    public MovieAdapter(List<Movie> movies, RecyclerView movieRV, final OnLoadMoreListener onLoadMoreListener, OnMovieItemClickListener onMovieItemClickListener) {
        this.movies = movies;
        this.onMovieItemClickListener = onMovieItemClickListener;

        if (movieRV.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) movieRV
                    .getLayoutManager();
            movieRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.titleTV.setText(movie.getTitle());
        holder.ratingTV.setText("rating: ".concat(String.valueOf(movie.getRatinig())));
        //ToDo wrong logic for posterUrl , need documentation for this
        String posterUrl = movie.getPosterUrl(500);
        Picasso.get().load(posterUrl).into(holder.posterIV);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.titleTV)
        TextView titleTV;
        @BindView(R.id.ratingTV)
        TextView ratingTV;
        @BindView(R.id.posterIV)
        ImageView posterIV;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMovieItemClickListener.onClickMovie(movies.get(getAdapterPosition()));
                }
            });
        }

    }

    public void setLoaded() {
        loading = false;
    }
}
