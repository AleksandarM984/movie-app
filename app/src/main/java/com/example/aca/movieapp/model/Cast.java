package com.example.aca.movieapp.model;

public class Cast {

    private String character;
    private String name;

    public String getCharacter() {
        return character;
    }

    public String getName() {
        return name;
    }

    public String getActorNameWithCharacter() {
        return name.concat("/ ").concat(character);
    }
}
