package com.example.aca.movieapp.baseMVP;

public interface BaseView {
    void showLoadingProgress();
    void hideLoadingProgress();
    void showErrorLoadingDataMessage(String errorMessage);
}
