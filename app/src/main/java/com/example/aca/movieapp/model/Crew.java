package com.example.aca.movieapp.model;

public class Crew {

    private String department;
    private String name;

    public String getCrewNameWithDepartment() {
        return name.concat(" / ").concat(department);
    }
}
