package com.example.aca.movieapp.model;

import com.example.aca.movieapp.BuildConfig;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Movie implements Serializable{

    private int id;
    private String title;
    private String overview;
    @SerializedName("poster_path")
    private String relativePosterImgUrl;
    @SerializedName("vote_average")
    private float ratinig;

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPosterUrl(int size){
        return BuildConfig.BASE_IMAGE_URL.concat("/w").concat(String.valueOf(size)).concat(relativePosterImgUrl);
    }

    public String getOverview() {
        return overview;
    }

    public float getRatinig() {
        return ratinig;
    }
}
