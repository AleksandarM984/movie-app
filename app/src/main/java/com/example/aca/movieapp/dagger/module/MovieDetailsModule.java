package com.example.aca.movieapp.dagger.module;

import com.example.aca.movieapp.dagger.qualifier.ActivityScope;
import com.example.aca.movieapp.movieDetails.mvp.MovieDetailsContract;
import com.example.aca.movieapp.movieMVP.MovieContract;

import dagger.Module;
import dagger.Provides;

@Module
public class MovieDetailsModule {

    private MovieDetailsContract.View view;
    private MovieDetailsContract.Interactor interactor;

    public MovieDetailsModule(MovieDetailsContract.View view, MovieDetailsContract.Interactor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @ActivityScope
    @Provides
    MovieDetailsContract.View provideView(){
        return this.view;
    }

    @ActivityScope
    @Provides
    MovieDetailsContract.Interactor provideInteractor(){
        return this.interactor;
    }
}
