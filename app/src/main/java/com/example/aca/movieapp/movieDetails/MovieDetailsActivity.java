package com.example.aca.movieapp.movieDetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aca.movieapp.MainActivity;
import com.example.aca.movieapp.MovieApplication;
import com.example.aca.movieapp.R;
import com.example.aca.movieapp.dagger.component.DaggerMovieDetailsComponent;
import com.example.aca.movieapp.dagger.component.NetComponent;
import com.example.aca.movieapp.dagger.module.MovieDetailsModule;
import com.example.aca.movieapp.model.Cast;
import com.example.aca.movieapp.model.Crew;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.MovieDetails;
import com.example.aca.movieapp.movieDetails.mvp.MovieDetailsContract;
import com.example.aca.movieapp.movieDetails.mvp.MovieDetailsInteractor;
import com.example.aca.movieapp.movieDetails.mvp.MovieDetailsPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MovieDetailsActivity extends AppCompatActivity implements MovieDetailsContract.View {

    private Movie movie;

    @BindView(R.id.titleTV)
    TextView titleTV;
    @BindView(R.id.overviewTV)
    TextView overviewTV;
    @BindView(R.id.castsContainerTV)
    TextView castsContainerTV;
    @BindView(R.id.crewsContainerTV)
    TextView crewsContainerTV;
    @BindView(R.id.favoriteIV)
    ImageView favoriteIV;

    @Inject
    MovieDetailsPresenter movieDetailsPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);
        if(getIntent() != null) {
            movie = (Movie) getIntent().getSerializableExtra(MainActivity.MOVIE);
            resolveDaggerInjection();
            showBasicMovieInfo(movie);
            movieDetailsPresenter.getMovieDetails();
        }
    }


    @Override
    public void onDetailsLoaded(MovieDetails movieDetails) {
        if(movieDetails.getCasts().size() > 0)
            showCasts(movieDetails.getCasts());

        if(movieDetails.getCrews().size() > 0)
            showCrews(movieDetails.getCrews());

    }


    @Override
    public void showLoadingProgress() {

    }

    @Override
    public void hideLoadingProgress() {

    }

    @Override
    public void showErrorLoadingDataMessage(String errorMessage) {

    }

    @OnClick(R.id.favoriteIV)
    public void signMovieLikeFavorite(){
        Toast.makeText(this, "Use markLikeFavorite request. Cant access account id to use post request for update Movie like favorite", Toast.LENGTH_LONG).show();
        favoriteIV.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.favorite));

    }
    private void resolveDaggerInjection() {
        NetComponent netComponent = ((MovieApplication) getApplication()).getNetComponent();
        DaggerMovieDetailsComponent.builder()
                .netComponent(netComponent)
                .movieDetailsModule(new MovieDetailsModule(this, new MovieDetailsInteractor()))
                .build()
                .inject(this);
    }

    private void showBasicMovieInfo(Movie movie) {
        titleTV.setText(movie.getTitle());
        overviewTV.setText(movie.getOverview());
    }

    private void showCasts(List<Cast> casts) {
        StringBuilder castsContent = new StringBuilder("");
        for (Cast cast : casts){
            String lineContent = cast.getActorNameWithCharacter().concat("\n");
            castsContent.append(lineContent);
        }
        castsContainerTV.setText(castsContent);
    }

    private void showCrews(List<Crew> casts) {
        StringBuilder crewContent = new StringBuilder("");
        for (Crew crew : casts){
            String lineContent = crew.getCrewNameWithDepartment().concat("\n");
            crewContent.append(lineContent);
        }
        crewsContainerTV.setText(crewContent);
    }

}
