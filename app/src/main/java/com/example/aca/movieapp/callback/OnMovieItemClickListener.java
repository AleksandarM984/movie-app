package com.example.aca.movieapp.callback;

import com.example.aca.movieapp.model.Movie;

public interface OnMovieItemClickListener {

    void onClickMovie(Movie movie);
}
