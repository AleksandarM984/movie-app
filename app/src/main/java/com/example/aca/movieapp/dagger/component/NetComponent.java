package com.example.aca.movieapp.dagger.component;

import com.example.aca.movieapp.MainActivity;
import com.example.aca.movieapp.dagger.module.AppModule;
import com.example.aca.movieapp.dagger.module.NetModule;
import com.example.aca.movieapp.dagger.qualifier.ApplicationScope;

import dagger.Component;
import retrofit2.Retrofit;

@ApplicationScope
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    Retrofit expose();
}
