package com.example.aca.movieapp.callback;

public interface OnLoadMoreListener {
    void onLoadMore();
}
