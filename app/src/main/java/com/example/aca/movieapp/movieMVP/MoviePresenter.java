package com.example.aca.movieapp.movieMVP;

import com.example.aca.movieapp.baseMVP.BasePresenter;
import com.example.aca.movieapp.baseMVP.BasePresenterImpl;
import com.example.aca.movieapp.model.Movie;
import com.example.aca.movieapp.model.SearchMovie;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class MoviePresenter extends BasePresenterImpl implements MovieContract.Presenter, MovieContract.Interactor.OnMovieResponseListener {

    private MovieContract.View movieView;

    @Inject
    Retrofit retrofit;
    @Inject
    MovieInteractor movieInteractor;

    @Inject
    public MoviePresenter(MovieContract.View movieView) {
        super(movieView);
        this.movieView = movieView;
    }

    @Override
    public void getTopRatedMovie() {
        movieView.showLoadingProgress();
        movieInteractor.getTopRatedMovie(this);
    }

    @Override
    public void onSuccessLoadedMovie(List<Movie> movies) {
        movieView.onTopRatedMovieLoaded(movies);
        movieView.hideLoadingProgress();
    }

    @Override
    public void onSuccessSearchMovie(List<SearchMovie> movies) {
        movieView.hideLoadingProgress();
        movieView.onSearchMovieLoaded(movies);
    }

    @Override
    public void onUnSuccessLoadedMovie(String error) {
        errorMsg(error);
    }

    public void searchMovie(String queryValue) {
        movieInteractor.searchMovie(queryValue, this);
    }
}
