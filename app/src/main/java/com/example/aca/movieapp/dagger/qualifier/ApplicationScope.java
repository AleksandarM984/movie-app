package com.example.aca.movieapp.dagger.qualifier;

import javax.inject.Scope;

@Scope
public @interface ApplicationScope {
}
