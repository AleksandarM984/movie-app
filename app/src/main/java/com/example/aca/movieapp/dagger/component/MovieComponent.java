package com.example.aca.movieapp.dagger.component;

import com.example.aca.movieapp.MainActivity;
import com.example.aca.movieapp.dagger.module.MovieModule;
import com.example.aca.movieapp.dagger.qualifier.ActivityScope;
import com.example.aca.movieapp.dagger.qualifier.ApplicationScope;
import com.example.aca.movieapp.movieMVP.MoviePresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = NetComponent.class, modules = {MovieModule.class})
public interface MovieComponent {
    MoviePresenter expose();
    void inject(MainActivity mainActivity);
}
