package com.example.aca.movieapp.model;

import java.util.List;

public class MovieDetails {
    private int id;
    private List<Crew> crew;
    private List<Cast> cast;

    public int getId() {
        return id;
    }

    public List<Crew> getCrews() {
        return crew;
    }

    public List<Cast> getCasts() {
        return cast;
    }
}
